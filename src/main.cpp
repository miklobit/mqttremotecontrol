// WIFI credentials
// #define WIFI_PASSWORD
// #define WIFI_SSID
#include "credentials.h"


#include <WiFi.h>
extern "C" {
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
}
#include <AsyncMqttClient.h>
#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>
#include "Adafruit_MCP23017.h"

#include "math.h"

#define MQTT_HOST IPAddress(192, 168, 178, 64)
#define MQTT_PORT 1883

#define DEADZONE                0.15    // 50% Deadzone
#define ANALOGTIMEOUT   100             // Refresh every 100ms

#define DEBOUNCETIMEOUT 20              // 20ms debounce time for buttons

#define MAX_LCD_CONTENT_LENGTH  83

#define AXIS_LX         0
#define AXIS_RX         1
#define AXIS_LY         2
#define AXIS_RY         3


// Message queue
QueueHandle_t queue;

#define ACTION_LCD_CONTENT      0
#define ACTION_SET_CURSOR       1
#define ACTION_BLINK            2
#define ACTION_HOME                     3
#define ACTION_CLEAR            4

struct queueStruct {
	uint8_t action;

	char	lcdContent[MAX_LCD_CONTENT_LENGTH];
	uint8_t lcdContentLength;

	uint8_t cursorCol = 0;
	uint8_t cursorRow = 0;

	boolean blink;
} outputQueueStruct, inputQueueStruct;


const String MQTT_BASE_TOPIC = "controller";
const String lcd_write_topic = MQTT_BASE_TOPIC + "/LCD/write";

const String blinkOff_write_topic = MQTT_BASE_TOPIC + "/LCD/blinkOff";
const String setCursor_write_topic = MQTT_BASE_TOPIC + "/LCD/setCursor";
const String blinkOn_write_topic = MQTT_BASE_TOPIC + "/LCD/blinkOn";
const String home_write_topic = MQTT_BASE_TOPIC + "/LCD/home";
const String clear_write_topic = MQTT_BASE_TOPIC + "/LCD/clear";

// const String cursorFull_write_topic = MQTT_BASE_TOPIC + "/LCD/cursorFull";
// const String setBacklight_write_topic = MQTT_BASE_TOPIC +
// "/LCD/setBacklight";

AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;
Adafruit_MCP23017 mcp;
LiquidCrystal_PCF8574 lcd(0x27);



// ************* Analog sticks
// LXmax = 4095;
// LYmax = 4095;
// RXmax = 3771;
// RYmax = 3751;
// LXmin = 192;
// LYmin = 64;
// RXmin = 16;
// RYmin = 0;

uint32_t analogTimestamp = millis();
uint32_t debounceTimestamp = millis();

struct axis {
	String		name;
	String		joystickName;
	String		mqttTopicValue;
	String		mqttTopicEvent;
	String		mqttTopicCenter;
	String		mqttTopicJoystickCenter;
	uint8_t		pin;
	uint16_t	min;
	uint16_t	max;
	uint16_t	deadzone;
	uint16_t	center;
	float		value;
	String		valueS;
	uint16_t	raw;
	boolean		dirty;
	boolean		centerDirty;
	float		invertFactor;
};

axis axes[4];

// Buttons
struct button {
	String	name;
	String	mqttTopicValue;
	String	mqttTopicRise;
	String	mqttTopicFall;
	boolean value;
	boolean lastValue;
	boolean dirty;
};

button buttons[22];
int digitalPins[] = { 4, 5, 16, 17, 18, 19 };

String lcdContent = "";
boolean lcdContentDirty = false;
boolean blinkOffDirty = false;
boolean setCursorDirty = false;
uint8_t setCursorCol = 0;
uint8_t setCursorRow = 0;

boolean blinkOnDirty = false;
boolean homeDirty = false;
boolean clearDirty = false;

// boolean cursorFullDirty = false;
// boolean setBacklightDirty = false;
// uint8_t backlight = 255;

/* Function prototypes */
void  updateButton(button& but, boolean val);

void  calcJoystic(axis& ax, axis& ay);
float calcAxis(axis & a, uint16_t value);

void  connectToWifi();
void  connectToMqtt();
void  WiFiEvent(WiFiEvent_t event);
void  onMqttConnect(bool sessionPresent);
void  onMqttDisconnect(AsyncMqttClientDisconnectReason reason);
void  onMqttSubscribe(uint16_t packetId, uint8_t qos);
void  onMqttUnsubscribe(uint16_t packetId);
void  onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total);
void  onMqttPublish(uint16_t packetId);

void setupMQTT();
void setupLCD();
void setupButtons();
void setupAnalogAxis();

void taskHandleQueue(void *parameter);

void setup() {
	Serial.begin(115200);

	queue = xQueueCreate(20, sizeof(queueStruct));
	if (queue == NULL) {
		Serial.println("Error creating the queue");
	}
	setupLCD();
	setupButtons();

	setupAnalogAxis();
	setupMQTT();

	xTaskCreate(
		taskHandleQueue,        /* Task function. */
		"taskHandleQueue",      /* String with name of task. */
		10000,                  /* Stack size in bytes. */
		NULL,                   /* Parameter passed as input of the task */
		31,                     /* Priority of the task. */
		NULL);                  /* Task handle. */
}

uint8_t row = 0;

void loop() {
	delay(10000);
}

/*
 * FUNCTIONS
 */
void setupMQTT() {
	mqttReconnectTimer = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdFALSE, (void *)0, reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));
	wifiReconnectTimer = xTimerCreate("wifiTimer", pdMS_TO_TICKS(2000), pdFALSE, (void *)0, reinterpret_cast<TimerCallbackFunction_t>(connectToWifi));

	WiFi.onEvent(WiFiEvent);

	mqttClient.onConnect(onMqttConnect);
	mqttClient.onDisconnect(onMqttDisconnect);
	mqttClient.onSubscribe(onMqttSubscribe);
	mqttClient.onUnsubscribe(onMqttUnsubscribe);
	mqttClient.onMessage(onMqttMessage);
	mqttClient.onPublish(onMqttPublish);
	mqttClient.setServer(MQTT_HOST, MQTT_PORT);

	connectToWifi();
}

void setupLCD() {
	int error;

	Wire.begin();
	Wire.beginTransmission(0x27);
	error = Wire.endTransmission();

	if (error == 0) {
		lcd.begin(20, 4); // initialize the lcd
	} else {
		// TODO: Error handling
	}

	lcd.setBacklight(255);
	lcd.home();
	lcd.clear();
}

void setupButtons() {
	mcp.begin(); // use default address 0

	for (size_t i = 0; i < 16; i++) {
		mcp.pinMode(i, INPUT);
		mcp.pullUp(i, HIGH);
		buttons[i].value = false;
		buttons[i].dirty = false;
	}

	buttons[0].name = "UP";
	buttons[1].name = "RIGHT";
	buttons[2].name = "HOME";
	buttons[3].name = "L3";
	buttons[4].name = "L2";
	buttons[5].name = "R1";
	buttons[6].name = "A";
	buttons[7].name = "B";
	buttons[8].name = "START";
	buttons[9].name = "Y";
	buttons[10].name = "SELECT";
	buttons[11].name = "DOWN";
	buttons[12].name = "PLAY";
	buttons[13].name = "R3";
	buttons[14].name = "NEXT";
	buttons[15].name = "L1";

	buttons[16].name = "X";
	buttons[17].name = "PLUS";
	buttons[18].name = "LEFT";
	buttons[19].name = "PREV";
	buttons[20].name = "R2";
	buttons[21].name = "MINUS";

	for (size_t i = 0; i < 22; i++) {
		buttons[i].mqttTopicValue = MQTT_BASE_TOPIC + '/' + buttons[i].name + "/read/value";
		buttons[i].mqttTopicRise = MQTT_BASE_TOPIC + '/' + buttons[i].name + "/read/rise";
		buttons[i].mqttTopicFall = MQTT_BASE_TOPIC + '/' + buttons[i].name + "/read/fall";
	}

	pinMode(19, INPUT_PULLUP);
	pinMode(18, INPUT_PULLUP);
	pinMode(5, INPUT_PULLUP);
	pinMode(17, INPUT_PULLUP);
	pinMode(16, INPUT_PULLUP);
	pinMode(4, INPUT_PULLUP);
}

void setupAnalogAxis() {
	axes[0].name = "LX";
	axes[0].joystickName = "L";
	axes[0].max = 4095;
	axes[0].min = 192;
	axes[0].invertFactor = 1;

	axes[1].name = "RX";
	axes[1].joystickName = "R";
	axes[1].max = 3771;
	axes[1].min = 16;
	axes[1].invertFactor = -1;

	axes[2].name = "LY";
	axes[2].joystickName = "L";
	axes[2].max = 4095;
	axes[2].min = 64;
	axes[2].invertFactor = 1;

	axes[3].name = "RY";
	axes[3].joystickName = "R";
	axes[3].max = 3751;
	axes[3].min = 0;
	axes[3].invertFactor = 1;

	for (size_t i = 0; i < 4; i++) {
		axes[i].value = 0.0;
		axes[i].valueS = "0.0";
		axes[i].pin = i + 32;
		axes[i].center = ((axes[i].max - axes[i].min) / 2);
		axes[i].deadzone = (axes[i].max - axes[i].center) * DEADZONE;
		axes[i].raw = 0;
		// axes[i].mqttTopicValue = MQTT_BASE_TOPIC + '/' + axes[i].name + "/value";
		axes[i].mqttTopicEvent = MQTT_BASE_TOPIC + '/' + axes[i].name + "/event";
		axes[i].mqttTopicCenter = MQTT_BASE_TOPIC + '/' + axes[i].name + "/center";
		axes[i].mqttTopicJoystickCenter = MQTT_BASE_TOPIC + '/' + axes[i].joystickName + "/center";
		axes[i].centerDirty = false;
		axes[i].dirty = false;
	}
}

/* calculate float value on single axis without deadzones */
float calcAxis(axis& a, uint16_t value) {
	if (value > a.center) {         // positive
		return (float)(value - a.center) / (a.max - a.center);
	} else if (value < a.center) {  // negative
		return (float)(value - a.center) / (a.center - a.min);
	} else {
		return 0.0;
	}
}

void calcJoystic(axis& ax, axis& ay) {
	uint16_t rawX = analogRead(ax.pin);
	uint16_t rawY = analogRead(ay.pin);
	float oldValueX = ax.value;
	float oldValueY = ay.value;

	ax.raw = rawX;
	ay.raw = rawY;
	float valueX = calcAxis(ax, rawX);
	float valueY = calcAxis(ay, rawY);

	if (abs(valueX) > DEADZONE) {
		if (valueX > 0) {
			ax.value = (float)((rawX - ax.center) - ax.deadzone) / ((ax.max - ax.center) - ax.deadzone);
		} else if (valueX < 0) {
			ax.value = (1.0 - ((float)(rawX - ax.min) / ((ax.center - ax.min) - ax.deadzone))) * -1;
		} else {
			ax.value = 0.0;
		}
		if (ax.value > 1) {
			ax.value = 1;
		}
		if (ax.value < -1) {
			ax.value = -1;
		}
		ax.value *= ax.invertFactor;
	} else {
		ax.value = 0.0;
	}

	if (abs(valueY) > DEADZONE) {
		if (valueY > 0) {
			ay.value = (float)((rawY - ay.center) - ay.deadzone) / ((ay.max - ay.center) - ay.deadzone);
		} else if (valueY < 0) {
			ay.value = (1.0 - ((float)(rawY - ay.min) / ((ay.center - ay.min) - ay.deadzone))) * -1;
		} else {
			ay.value = 0.0;
		}
		if (ay.value > 1) {
			ay.value = 1;
		}
		if (ay.value < -1) {
			ay.value = -1;
		}
		ay.value *= ay.invertFactor;
	} else {
		ay.value = 0.0;
	}

	// rounding
	ax.value = (int)(ax.value * 100.0) / -100.0;
	ay.value = (int)(ay.value * 100.0) / -100.0;

	if (oldValueX != ax.value) {
		ax.dirty = true;
	}
	if (oldValueY != ay.value) {
		ay.dirty = true;
	}

	if ((oldValueX != 0.0) && (ax.value == 0.0)) {
		ax.centerDirty = true;
	}
	if ((oldValueY != 0.0) && (ay.value == 0.0)) {
		ay.centerDirty = true;
	}

	ax.valueS = String(ax.value);
	ay.valueS = String(ay.value);
}


void taskHandleQueue(void *parameter) {
	while (true) {
		if (xQueueReceive(queue, &outputQueueStruct, 1)) {
			// Serial.println("MESSAGE RECEIVED");
			// Serial.print("loop on core ");
			// Serial.println(xPortGetCoreID());

			switch (outputQueueStruct.action) {
			case ACTION_LCD_CONTENT:
				// Serial.println("ACTION_LCD_CONTENT");

				row = 0;

				lcd.setCursor(0, row);
				for (size_t i = 0; i < outputQueueStruct.lcdContentLength; i++) {
					if (outputQueueStruct.lcdContent[i] == '\n') {
						row++;

						if (row > 3) {
							return;
						}
						lcd.setCursor(0, row);
					} else {
						lcd.print(outputQueueStruct.lcdContent[i]);
					}
				}
				break;
			case ACTION_SET_CURSOR:
				// Serial.println("ACTION_SET_CURSOR");
				lcd.setCursor(outputQueueStruct.cursorCol, outputQueueStruct.cursorRow);
				break;
			case ACTION_BLINK:
				// Serial.println("ACTION_BLINK");
				if (outputQueueStruct.blink) {
					lcd.blink();
				} else {
					lcd.noBlink();
				}
				break;
			case ACTION_HOME:
				// Serial.println("ACTION_HOME");
				lcd.home();
				break;
			case ACTION_CLEAR:
				// Serial.println("ACTION_CLEAR");
				lcd.clear();
				break;
			default:
				// Serial.println("DIFFERENT ACTION");
				break;
			}
		}

		if (millis() > (debounceTimestamp + DEBOUNCETIMEOUT)) {
			debounceTimestamp = millis();

			for (size_t i = 0; i < 16; i++)
				updateButton(buttons[i], mcp.digitalRead(i));

			for (size_t i = 0; i < 6; i++)
				updateButton(buttons[i + 16], digitalRead(digitalPins[i]));

			for (size_t i = 0; i < 22; i++) {
				if (buttons[i].dirty) {
					buttons[i].dirty = false;
					mqttClient.publish(buttons[i].mqttTopicValue.c_str(), 0, false, (buttons[i].value ? "1" : "0"));

					if (buttons[i].value) { // true => rising
						mqttClient.publish(buttons[i].mqttTopicRise.c_str(), 0, false, "1");
					} else {
						mqttClient.publish(buttons[i].mqttTopicFall.c_str(), 0, false, "1");
					}
				}
			}
		}

		if (millis() > (analogTimestamp + ANALOGTIMEOUT)) {
			analogTimestamp = millis();
			calcJoystic(axes[0], axes[2]);
			calcJoystic(axes[1], axes[3]);

			for (size_t i = 0; i < 4; i++) {
				// mqttClient.publish(axes[i].mqttTopicValue.c_str(), 0, false, axes[i].valueS.c_str());

				if (axes[i].dirty) {
					axes[i].dirty = false;
					mqttClient.publish(axes[i].mqttTopicEvent.c_str(), 0, false, axes[i].valueS.c_str());
				}
			}

			if (axes[AXIS_LX].centerDirty || axes[AXIS_LY].centerDirty) {
				mqttClient.publish(axes[AXIS_LX].mqttTopicJoystickCenter.c_str(), 0, false, "1");
			}

			if (axes[AXIS_LX].centerDirty) {
				axes[AXIS_LX].centerDirty = false;
				mqttClient.publish(axes[AXIS_LX].mqttTopicCenter.c_str(), 0, false, "1");
			}

			if (axes[AXIS_LY].centerDirty) {
				axes[AXIS_LY].centerDirty = false;
				mqttClient.publish(axes[AXIS_LY].mqttTopicCenter.c_str(), 0, false, "1");
			}

			if (axes[AXIS_RX].centerDirty || axes[AXIS_RY].centerDirty) {
				mqttClient.publish(axes[AXIS_RX].mqttTopicJoystickCenter.c_str(), 0, false, "1");
			}

			if (axes[AXIS_RX].centerDirty) {
				axes[AXIS_RX].centerDirty = false;
				mqttClient.publish(axes[AXIS_RX].mqttTopicCenter.c_str(), 0, false, "1");
			}

			if (axes[AXIS_RY].centerDirty) {
				axes[AXIS_RY].centerDirty = false;
				mqttClient.publish(axes[AXIS_RY].mqttTopicCenter.c_str(), 0, false, "1");
			}
		}
	}
}

void updateButton(button& but, boolean val) {
	if (but.value != val) {                 // value has changed
		if (but.lastValue == val) {     // debeunce
			but.value = val;
			but.dirty = true;
		}
		but.lastValue = val;
	}
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
	String payload_s(payload);
	String topic_s(topic);

	// Serial.print("onMqttMessage on core ");
	// Serial.println(xPortGetCoreID());

	if (topic_s == lcd_write_topic) {
		lcdContent = String(payload).substring(0, len);
		lcdContentDirty = true;

		inputQueueStruct.action = ACTION_LCD_CONTENT;

		if (len > MAX_LCD_CONTENT_LENGTH) {
			inputQueueStruct.lcdContentLength = MAX_LCD_CONTENT_LENGTH;
		} else {
			inputQueueStruct.lcdContentLength = len;
		}

		strncpy(inputQueueStruct.lcdContent, payload, inputQueueStruct.lcdContentLength);
	} else if (topic_s == setCursor_write_topic) {
		setCursorCol = (uint8_t)payload[0];
		setCursorRow = (uint8_t)payload[1];
		setCursorDirty = true;

		inputQueueStruct.cursorCol = (uint8_t)payload[0];
		inputQueueStruct.cursorRow = (uint8_t)payload[1];
		inputQueueStruct.action = ACTION_SET_CURSOR;
	} else if (topic_s == blinkOff_write_topic) {
		inputQueueStruct.blink = false;
		inputQueueStruct.action = ACTION_BLINK;
		blinkOffDirty = true;
	} else if (topic_s == blinkOn_write_topic) {
		inputQueueStruct.blink = true;
		inputQueueStruct.action = ACTION_BLINK;
		blinkOnDirty = true;
	} else if (topic_s == home_write_topic) {
		inputQueueStruct.action = ACTION_HOME;
		homeDirty = true;
	} else if (topic_s == clear_write_topic) {
		inputQueueStruct.action = ACTION_CLEAR;
		clearDirty = true;

		// }else if( topic_s == cursorFull_write_topic ) {
		//  cursorFullDirty = true;
		// }else if( topic_s == setBacklight_write_topic ) {
		//  backlight = payload[0];
		//  setBacklightDirty = true;
	}
	xQueueSend(queue, &inputQueueStruct, 1);
}

void connectToWifi() {
	// Serial.println("Connecting to Wi-Fi...");
	WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt() {
	// Serial.println("Connecting to MQTT...");
	mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event) {
	// Serial.printf("[WiFi-event] event: %d\n", event);
	switch (event) {
	case SYSTEM_EVENT_STA_GOT_IP:

		// Serial.println("WiFi connected");
		// Serial.println("IP address: ");
		// Serial.println(WiFi.localIP());
		connectToMqtt();
		break;

	case SYSTEM_EVENT_STA_DISCONNECTED:

		// Serial.println("WiFi lost connection");
		xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT
		// while reconnecting to Wi-Fi
		xTimerStart(wifiReconnectTimer, 0);
		break;

	default:
		break;
	}
}

void onMqttConnect(bool sessionPresent) {
	// Serial.println("Connected to MQTT.");
	// Serial.print("Session present: ");
	// Serial.println(sessionPresent);
	mqttClient.subscribe(lcd_write_topic.c_str(), 0);
	mqttClient.subscribe(setCursor_write_topic.c_str(), 0);
	mqttClient.subscribe(blinkOff_write_topic.c_str(), 0);
	mqttClient.subscribe(blinkOn_write_topic.c_str(), 0);
	mqttClient.subscribe(home_write_topic.c_str(), 0);
	mqttClient.subscribe(clear_write_topic.c_str(), 0);

	// mqttClient.subscribe( cursorFull_write_topic.c_str(),0);
	// mqttClient.subscribe( setBacklight_write_topic.c_str(),0);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
	// Serial.println("Disconnected from MQTT.");

	if (WiFi.isConnected()) {
		xTimerStart(mqttReconnectTimer, 0);
	}
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
	// Serial.println("Subscribe acknowledged.");
	// Serial.print("  packetId: ");
	// Serial.println(packetId);
	// Serial.print("  qos: ");
	// Serial.println(qos);
}

void onMqttUnsubscribe(uint16_t packetId) {
	// Serial.println("Unsubscribe acknowledged.");
	// Serial.print("  packetId: ");
	// Serial.println(packetId);
}

void onMqttPublish(uint16_t packetId) {
	// Serial.println("Publish acknowledged.");
	// Serial.print("  packetId: ");
	// Serial.println(packetId);
}
